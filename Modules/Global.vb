#Region "Init"
Option Strict On
Option Explicit On
#End Region

Module [Global]
#Region "Variables"
	Public gsAppPath, gsVersion, gsWindowsUserID, gsLogPath, gsEMailRecipient, gsEMailSender, gsHSXConnectionString, gsEMRConnectionString, gsHSXFileLocation, gsLogFileName, gsEventLogName As String
	Public gsHSXPanelsFileLocation, gsControllerConnectionString As String
	Public gbTrace, gbSendEMail As Boolean
	Public giServicePauseTime, giHSXPanelsProcessingMonthDay As Int32
	Public galHSXPanelsProcessingTime As ArrayList
#End Region

#Region "Constants"
	Public Const gsApplicationName As String = "HSX"
	Public Const gsEntityDescription As String = "Crozer-Keystone Health System, Inc."
	Public Const giEMailPauseTime As Int32 = 2000
	Public Const gsSMTPAddress As String = "smtp.crozer.org"
	Public Const giAccountID As Int32 = 1
	Public Const giSQLTimeOutNormal As Int32 = 30
	Public Const giSQLTimeOutExtended As Int32 = 300
	Public Const gsEMRServer As String = "SQLDB04"
	Public Const gsEMRDatabase As String = "EMR01"
#End Region
End Module