#Region "Init"
Option Explicit On
Option Strict On
Imports System.IO
Imports System.IO.Path
Imports Microsoft.VisualBasic.FileIO
Imports System.Data.OleDb
#End Region

Module ServiceRoutines
#Region "Declarations"
	Private msErrorMessage, msFontName As String
	Private miSequenceNumber, miRow, miFontSize, miColor, miColorHold As Int32
  Private miBold As Boolean
  Private mdtCurrentDateTime As Date
	Private moProcessHistory As New ProcessHistory
#End Region

#Region "Routines"
	Private Function DownloadFileCount(DownloadFileLocation As String, ProcessFileName As String) As Int32
		Dim sFiles() As String = IO.Directory.GetFiles(DownloadFileLocation, ProcessFileName)

		Return sFiles.Count
	End Function

	Private Sub AddField(ByRef OutRecord As String, Field As String, MaximumLength As Int32, AddDoubleQuotes As Boolean)
		Dim sField As String = Field

		If MaximumLength > 0 Then
			If Len(Field) > MaximumLength Then Field = Mid(Field, 1, MaximumLength)
		End If
		If Len(OutRecord) > 0 Then OutRecord = OutRecord & ","
		If AddDoubleQuotes Then
			OutRecord = OutRecord & Chr(34) & sField & Chr(34)
		Else
			OutRecord = OutRecord & sField
		End If
	End Sub
#End Region

#Region "StartService"
	Public Sub StartService(InTextWriterTraceListener As TextWriterTraceListener)
		Dim sRoutine As String = "StartService"
		Dim oFile As StreamWriter = Nothing
		Dim oServicesToRun() As System.ServiceProcess.ServiceBase
		Dim oStartup As New StartUp

		WriteEventLog("Service main method starting at " & Format$(Now, "MM/dd/yyyy HH:mm"), sRoutine, EventLogEntryType.Information)
		If oStartup.Run Then
			CreateDirectory(gsLogPath)
			gsLogFileName = gsApplicationName & " Log " & Format$(Now, "yyyyMMdd HHmmss") & ".txt"
			oFile = File.CreateText(gsLogPath & gsLogFileName)
			InTextWriterTraceListener = New TextWriterTraceListener(oFile)
			Trace.Listeners.Add(InTextWriterTraceListener)
			Trace.AutoFlush = True
			TraceWrite("Startup.Run was initialized...", sRoutine)
			TraceWrite("Application Name: " & gsApplicationName, sRoutine)
			TraceWrite("Windows UserID: " & gsWindowsUserID, sRoutine)
			TraceWrite("Entity Description: " & gsEntityDescription, sRoutine)
			TraceWrite("Application Version: " & gsVersion, sRoutine)
			TraceWrite("Application Startup Path: " & gsAppPath, sRoutine)
			TraceWrite("Service Pause Time: " & Format$(giServicePauseTime \ 60000, "#,##0") & " minutes", sRoutine)
			TraceWrite("EMail Recipient: " & gsEMailRecipient, sRoutine)
			TraceWrite("EMail Sender: " & gsEMailSender, sRoutine)
			If gbSendEMail Then
				TraceWrite("Send EMail: True", sRoutine)
			Else
				TraceWrite("Send EMail: False", sRoutine)
			End If
			TraceWrite("HSX File Location: " & gsHSXFileLocation, sRoutine)
			TraceWrite("HSX Panels File Location: " & gsHSXPanelsFileLocation, sRoutine)
			If galHSXPanelsProcessingTime.Count > 0 Then
				For iWork = 0 To galHSXPanelsProcessingTime.Count - 1
					TraceWrite("HSX Panels Processing Time(" & Format$(iWork + 1, "#,##0") & "): " & galHSXPanelsProcessingTime.Item(iWork).ToString, sRoutine)
				Next iWork
			End If
			TraceWrite("HSX Panels Processing Month Day: " & giHSXPanelsProcessingMonthDay, sRoutine)

			If Not gbTrace Then
				gbTrace = True
				TraceWrite("Trace is disabled", sRoutine)
				gbTrace = False
			End If
			WriteEventTrace("Startup Complete", sRoutine, EventLogEntryType.Information)
			TraceWrite(StringRepeat(Chr(127), 100), sRoutine)
			TraceWrite(StringRepeat("-", 100), sRoutine)
			TraceWrite(StringRepeat(Chr(127), 100), sRoutine)
			TraceWrite(Space$(1), sRoutine)
		Else
			WriteEventTrace("Startup.Run failed to initialize...", sRoutine, EventLogEntryType.Error)
			WriteEventTrace(oStartup.ErrorMessage, sRoutine, EventLogEntryType.Error)
			End
		End If
		oStartup = Nothing
		oServicesToRun = New System.ServiceProcess.ServiceBase() {New HSX}
		System.ServiceProcess.ServiceBase.Run(oServicesToRun)
		WriteEventTrace("Service main method exiting...", sRoutine, EventLogEntryType.Information)
		Trace.Listeners.Remove(InTextWriterTraceListener)
		InTextWriterTraceListener.Close()
		InTextWriterTraceListener = Nothing
		oFile.Close()
	End Sub
#End Region

#Region "Load Data Routines"
	Public Function LoadData(ProcessTypeID As Int32) As Boolean
		Dim sRoutine As String = "LoadData"
		Dim sFiles(), sFile, sProcessFilePattern, sProcessedFileLocation, sDownloadFileLocation, sFileNameOnly As String
		Dim iHSXRecordTypeID As Int32

		Try
			mdtCurrentDateTime = Now
			sDownloadFileLocation = AddBackslash(gsHSXFileLocation) & "Download\"
			sProcessedFileLocation = AddBackslash(gsHSXFileLocation) & "Processed\"
			CreateDirectory(sProcessedFileLocation)
			Select Case ProcessTypeID
				Case 1
					sProcessFilePattern = "CKHS_AETNAMCARE_*.csv"
					iHSXRecordTypeID = 1
				Case 2
					sProcessFilePattern = "CKHN_ENS_*.csv"
					iHSXRecordTypeID = 2
				Case 3
					sProcessFilePattern = "CKHS_ENS_FacilityAdmits_*.csv"
					iHSXRecordTypeID = 3
				Case 4
					sProcessFilePattern = "CKHS_IPPIP_*.csv"
					iHSXRecordTypeID = 4
				Case 5
					sProcessFilePattern = "CKHS_RTMAFULL_*.csv"
					iHSXRecordTypeID = 5
				Case 6
					sProcessFilePattern = "CROMSSP_Summary_*.csv"
					iHSXRecordTypeID = 6
				Case Else
					Throw New Exception("ProcessTypeID " & ProcessTypeID.ToString & " is invalid.")
			End Select
			TraceWrite("Start Processing" & sProcessFilePattern, sRoutine)
			sFiles = IO.Directory.GetFiles(sDownloadFileLocation, sProcessFilePattern)
			If sFiles.Count > 0 Then
				For Each sFile In sFiles
					If Not moProcessHistory.MarkStart(ProcessTypeID, mdtCurrentDateTime, sFile) Then Throw New Exception(moProcessHistory.ErrorMessage)
					If ImportData(sFile, iHSXRecordTypeID) Then
						sFileNameOnly = GetFileName(sFile)
						If File.Exists(sProcessedFileLocation & sFileNameOnly) Then DeleteFile(sProcessedFileLocation & sFileNameOnly)
						File.Move(sFile, sProcessedFileLocation & sFileNameOnly)
					Else
						Throw New Exception("Error importing data, processing canceled.")
					End If
					If Not moProcessHistory.MarkEnd(mdtCurrentDateTime) Then Throw New Exception(moProcessHistory.ErrorMessage)
				Next
			End If
			TraceWrite("End Processing" & sProcessFilePattern, sRoutine)
			LoadData = True
		Catch oException As Exception
			TraceWrite("Error: " & oException.Message, sRoutine)
			LoadData = False
		Finally
		End Try
	End Function

	Private Function ImportData(PathFileName As String, HSXRecordTypeID As Int32) As Boolean
		Dim sRoutine As String = "ImportData"
		Dim oHSX01 As New HSX01
		Dim sFileNameOnly As String = GetFileName(PathFileName)
		Dim sFileReader As String = Nothing
		Dim oFileStream As FileStream = Nothing
		Dim oStreamReader As StreamReader = Nothing
		Dim oTextFieldParser As TextFieldParser
		Dim sInRecord As String
		Dim iRecordsRead As Int32 = 0
		Dim iRecordsWritten As Int32 = 0
		Dim asFields() As String
		Dim oStringReader As StringReader
		Dim bParseError As Boolean
		Dim iFieldCount01 As Int32 = 31
		Dim iFieldCount02 As Int32 = 33
		Dim iFieldCount03 As Int32 = 33

		Try
			TraceWrite("Start HSX01 processing for file " & PathFileName, sRoutine)
			If Not File.Exists(PathFileName) Then Throw New Exception("Error: File " & PathFileName & "wasn't found.")
			sFileReader = FileSystem.ReadAllText(PathFileName).Replace(vbLf, Space(1))
			FileSystem.WriteAllText(PathFileName, sFileReader, False)
			oFileStream = New FileStream(PathFileName, FileMode.Open, FileAccess.Read)
			oStreamReader = New StreamReader(oFileStream)

			If Not oHSX01.OpenDatabase Then Throw New Exception("Error opening database, processing canceled")
			If Not oHSX01.Delete(HSXRecordTypeID, sFileNameOnly) Then Throw New Exception("Error removing existing records from source " & sFileNameOnly & ", processing canceled")
			While oStreamReader.Peek() > -1
			sInRecord = Trim$(oStreamReader.ReadLine())
				If Len(sInRecord) > 0 Then
					iRecordsRead += 1
					oStringReader = New StringReader(sInRecord)
					oTextFieldParser = New TextFieldParser(oStringReader)
					oTextFieldParser.TextFieldType = FieldType.Delimited
					oTextFieldParser.Delimiters = New String() {","}
					oTextFieldParser.HasFieldsEnclosedInQuotes = True

					Try
						asFields = oTextFieldParser.ReadFields()
						bParseError = False
					Catch oException As Exception
						asFields = Nothing
						bParseError = True
					Finally
					End Try

					If bParseError Then Throw New Exception("Parse Error In Record(" & iRecordsRead & "): " & sInRecord)
					Select Case HSXRecordTypeID
						Case 3
							If asFields.Count <> iFieldCount02 Then Throw New Exception(sRoutine & ": Expected field count of " & iFieldCount02 & ", received " & asFields.Count)
						Case 6
							If asFields.Count <> iFieldCount03 Then Throw New Exception(sRoutine & ": Expected field count of " & iFieldCount03 & ", received " & asFields.Count)
						Case Else
							If asFields.Count <> iFieldCount01 Then Throw New Exception(sRoutine & ": Expected field count of " & iFieldCount01 & ", received " & asFields.Count)
					End Select

					If iRecordsRead = 1 Then
						Select Case HSXRecordTypeID
							Case 3
								If LCase(asFields(0)) <> "destination facility" Or LCase(asFields(1)) <> "destination practice" Or LCase(asFields(2)) <> "primary care provider" Or LCase(asFields(3)) <> "destination mrn" Or LCase(asFields(4)) <> "source facility" Or
									LCase(asFields(5)) <> "source mrn" Or LCase(asFields(6)) <> "first name" Or LCase(asFields(7)) <> "middle name" Or LCase(asFields(8)) <> "last name" Or LCase(asFields(9)) <> "gender" Or
									LCase(asFields(10)) <> "date of birth" Or LCase(asFields(11)) <> "address" Or LCase(asFields(12)) <> "city" Or LCase(asFields(13)) <> "state" Or LCase(asFields(14)) <> "zip" Or
									LCase(asFields(15)) <> "primary phone" Or LCase(asFields(16)) <> "source setting" Or LCase(asFields(17)) <> "event type" Or LCase(asFields(18)) <> "roster added date" Or LCase(asFields(19)) <> "admit date" Or
									LCase(asFields(20)) <> "admit time" Or LCase(asFields(21)) <> "admit reason" Or LCase(asFields(22)) <> "admit type" Or LCase(asFields(23)) <> "referral information" Or LCase(asFields(24)) <> "discharge date" Or
									LCase(asFields(25)) <> "discharge time" Or LCase(asFields(26)) <> "death indicator" Or LCase(asFields(27)) <> "diagnosis code" Or LCase(asFields(28)) <> "diagnosis description" Or LCase(asFields(29)) <> "discharge disposition" Or
									LCase(asFields(30)) <> "attending doctor" Or LCase(asFields(31)) <> "insurance" Or LCase(asFields(32)) <> "subscriber list" Then Throw New Exception("Invalid data found in file " & PathFileName & ", Processing canceled.")

							Case 6
								If LCase(asFields(0)) <> "destination facility" Or LCase(asFields(1)) <> "destination practice" Or LCase(asFields(2)) <> "primary care provider" Or LCase(asFields(3)) <> "destination mrn" Or LCase(asFields(4)) <> "source facility" Or
									LCase(asFields(5)) <> "source mrn" Or LCase(asFields(6)) <> "first name" Or LCase(asFields(7)) <> "middle name" Or LCase(asFields(8)) <> "last name" Or LCase(asFields(9)) <> "gender" Or
									LCase(asFields(10)) <> "date of birth" Or LCase(asFields(11)) <> "address" Or LCase(asFields(12)) <> "city" Or LCase(asFields(13)) <> "state" Or LCase(asFields(14)) <> "zip" Or
									LCase(asFields(15)) <> "primary phone" Or LCase(asFields(16)) <> "source setting" Or LCase(asFields(17)) <> "event type" Or LCase(asFields(18)) <> "admit date" Or
									LCase(asFields(19)) <> "admit time" Or LCase(asFields(20)) <> "admit reason" Or LCase(asFields(21)) <> "admit type" Or LCase(asFields(22)) <> "referral information" Or LCase(asFields(23)) <> "discharge date" Or
									LCase(asFields(24)) <> "discharge time" Or LCase(asFields(25)) <> "death indicator" Or LCase(asFields(26)) <> "diagnosis code" Or LCase(asFields(27)) <> "diagnosis description" Or LCase(asFields(28)) <> "discharge disposition" Or
									LCase(asFields(29)) <> "attending doctor" Or LCase(asFields(30)) <> "insurance" Or LCase(asFields(31)) <> "care manager name" Or LCase(asFields(32)) <> "subscriber list" Then Throw New Exception("Invalid data found in file " & PathFileName & ", Processing canceled.")

								'Case 6
								'	If LCase(asFields(0)) <> "destination facility" Or LCase(asFields(1)) <> "destination practice" Or LCase(asFields(2)) <> "primary care provider" Or LCase(asFields(3)) <> "destination mrn" Or LCase(asFields(4)) <> "source facility" Or
								'		LCase(asFields(32)) <> "subscriber list" Then Throw New Exception("Invalid data found in file " & PathFileName & ", Processing canceled.")


							Case Else
								If LCase(asFields(0)) <> "destination facility" Or LCase(asFields(1)) <> "destination practice" Or LCase(asFields(2)) <> "primary care provider" Or LCase(asFields(3)) <> "destination mrn" Or LCase(asFields(4)) <> "source facility" Or
									LCase(asFields(5)) <> "source mrn" Or LCase(asFields(6)) <> "first name" Or LCase(asFields(7)) <> "middle name" Or LCase(asFields(8)) <> "last name" Or LCase(asFields(9)) <> "gender" Or
									LCase(asFields(10)) <> "date of birth" Or LCase(asFields(11)) <> "address" Or LCase(asFields(12)) <> "city" Or LCase(asFields(13)) <> "state" Or LCase(asFields(14)) <> "zip" Or
									LCase(asFields(15)) <> "primary phone" Or LCase(asFields(16)) <> "source setting" Or LCase(asFields(17)) <> "event type" Or LCase(asFields(18)) <> "admit date" Or LCase(asFields(19)) <> "admit time" Or
									LCase(asFields(20)) <> "admit reason" Or LCase(asFields(21)) <> "admit type" Or LCase(asFields(22)) <> "referral information" Or LCase(asFields(23)) <> "discharge date" Or LCase(asFields(24)) <> "discharge time" Or
									LCase(asFields(25)) <> "death indicator" Or LCase(asFields(26)) <> "diagnosis code" Or LCase(asFields(27)) <> "diagnosis description" Or LCase(asFields(28)) <> "discharge disposition" Or LCase(asFields(29)) <> "attending doctor" Or
									LCase(asFields(30)) <> "insurance" Then Throw New Exception("Invalid data found in file " & PathFileName & ", Processing canceled.")
						End Select
					Else
						With oHSX01
							.Initialize()
							.HSXRecordTypeID = HSXRecordTypeID
							Select Case HSXRecordTypeID
								Case 3
									.DestinationFacility = SetString(asFields(0))
									.DestinationPractice = SetString(asFields(1))
									.PrimaryCareProvider = SetString(asFields(2))
									.DestinationMRN = SetString(asFields(3))
									.SourceFacility = SetString(asFields(4))
									.SourceMRN = SetString(asFields(5))
									.FirstName = SetString(asFields(6))
									.MiddleName = SetString(asFields(7))
									.LastName = SetString(asFields(8))
									.Gender = SetString(asFields(9))
									.DateOfBirth = SetString(asFields(10))
									.Address = SetString(asFields(11))
									.City = SetString(asFields(12))
									.State = SetString(asFields(13))
									.Zip = SetString(asFields(14))
									.PrimaryPhone = SetString(asFields(15))
									.SourceSetting = SetString(asFields(16))
									.EventType = SetString(asFields(17))
									.RosterAddedDate = SetString(asFields(18))
									.AdmitDate = SetString(asFields(19))
									.AdmitTime = SetString(asFields(20))
									.AdmitReason = SetString(asFields(21))
									.AdmitType = SetString(asFields(22))
									.ReferralInformation = SetString(asFields(23))
									.DischargeDate = SetString(asFields(24))
									.DischargeTime = SetString(asFields(25))
									.DeathIndicator = SetString(asFields(26))
									.DiagnosisCode = SetString(asFields(27))
									.DiagnosisDescription = SetString(asFields(28))
									.DischargeDisposition = SetString(asFields(29))
									.AttendingDoctor = SetString(asFields(30))
									.Insurance = SetString(asFields(31))
									.CareManagerName = String.Empty
									.SubscriberList = SetString(asFields(32))

								Case 6
									.DestinationFacility = SetString(asFields(0))
									.DestinationPractice = SetString(asFields(1))
									.PrimaryCareProvider = SetString(asFields(2))
									.DestinationMRN = SetString(asFields(3))
									.SourceFacility = SetString(asFields(4))
									.SourceMRN = SetString(asFields(5))
									.FirstName = SetString(asFields(6))
									.MiddleName = SetString(asFields(7))
									.LastName = SetString(asFields(8))
									.Gender = SetString(asFields(9))
									.DateOfBirth = SetString(asFields(10))
									.Address = SetString(asFields(11))
									.City = SetString(asFields(12))
									.State = SetString(asFields(13))
									.Zip = SetString(asFields(14))
									.PrimaryPhone = SetString(asFields(15))
									.SourceSetting = SetString(asFields(16))
									.EventType = SetString(asFields(17))
									.RosterAddedDate = String.Empty
									.AdmitDate = SetString(asFields(18))
									.AdmitTime = SetString(asFields(19))
									.AdmitReason = SetString(asFields(20))
									.AdmitType = SetString(asFields(21))
									.ReferralInformation = SetString(asFields(22))
									.DischargeDate = SetString(asFields(23))
									.DischargeTime = SetString(asFields(24))
									.DeathIndicator = SetString(asFields(25))
									.DiagnosisCode = SetString(asFields(26))
									.DiagnosisDescription = SetString(asFields(27))
									.DischargeDisposition = SetString(asFields(28))
									.AttendingDoctor = SetString(asFields(29))
									.Insurance = SetString(asFields(30))
									.CareManagerName = SetString(asFields(31))
									.SubscriberList = SetString(asFields(32))

								Case Else
									.DestinationFacility = SetString(asFields(0))
									.DestinationPractice = SetString(asFields(1))
									.PrimaryCareProvider = SetString(asFields(2))
									.DestinationMRN = SetString(asFields(3))
									.SourceFacility = SetString(asFields(4))
									.SourceMRN = SetString(asFields(5))
									.FirstName = SetString(asFields(6))
									.MiddleName = SetString(asFields(7))
									.LastName = SetString(asFields(8))
									.Gender = SetString(asFields(9))
									.DateOfBirth = SetString(asFields(10))
									.Address = SetString(asFields(11))
									.City = SetString(asFields(12))
									.State = SetString(asFields(13))
									.Zip = SetString(asFields(14))
									.PrimaryPhone = SetString(asFields(15))
									.SourceSetting = SetString(asFields(16))
									.EventType = SetString(asFields(17))
									.RosterAddedDate = String.Empty
									.AdmitDate = SetString(asFields(18))
									.AdmitTime = SetString(asFields(19))
									.AdmitReason = SetString(asFields(20))
									.AdmitType = SetString(asFields(21))
									.ReferralInformation = SetString(asFields(22))
									.DischargeDate = SetString(asFields(23))
									.DischargeTime = SetString(asFields(24))
									.DeathIndicator = SetString(asFields(25))
									.DiagnosisCode = SetString(asFields(26))
									.DiagnosisDescription = SetString(asFields(27))
									.DischargeDisposition = SetString(asFields(28))
									.AttendingDoctor = SetString(asFields(29))
									.Insurance = SetString(asFields(30))
									.CareManagerName = String.Empty
									.SubscriberList = String.Empty
							End Select
							.SourceFileName = sFileNameOnly
							If Not .Insert Then Throw New Exception("Error inserting into HSX01: " & .ErrorMessage)
						End With
						iRecordsWritten += 1
					End If
				End If
			End While

			CloseInput(oStreamReader, oFileStream)
			oHSX01.CloseDatabase()
			moProcessHistory.MarkRecordCount(iRecordsRead, iRecordsWritten)
			ImportData = True
		Catch oException As Exception
			TraceWrite("Error: " & oException.Message, sRoutine)
			CloseInput(oStreamReader, oFileStream)
			oHSX01.CloseDatabase()
			ImportData = False
		Finally
		End Try
		TraceWrite("End HSX01 processing for file " & PathFileName, sRoutine)
	End Function
#End Region

#Region "HSX Panels Routines"
	Public Sub DoPanels()
		Dim sRoutine As String = "DoPanels"
		Dim oHSX01 As New HSX01
		Dim alHSXPanelsPracticeID As ArrayList
		Dim iWork, iHSXPanelsPracticeID As Int32
		Dim iProcessMonth, iProcessYear As Int32
		Dim sCurrentDate As String

		mdtCurrentDateTime = Now
		sCurrentDate = Format$(mdtCurrentDateTime, "MM/dd/yyyy")
		iProcessMonth = Month(DateAdd(DateInterval.Day, -1, mdtCurrentDateTime))
		iProcessYear = Year(DateAdd(DateInterval.Day, -1, mdtCurrentDateTime))
		If CDate(sCurrentDate & " " & galHSXPanelsProcessingTime.Item(iWork).ToString) <= Now Or Day(mdtCurrentDateTime) <giHSXPanelsProcessingMonthDay Then
			If oHSX01.EMRProcessed("daily", "Daily Complete") Then
				alHSXPanelsPracticeID = oHSX01.GetHSXPanelsPracticeID
				If alHSXPanelsPracticeID.Count <= 0 Then Throw New Exception("No practices found to process")
				For iWork = 0 To alHSXPanelsPracticeID.Count - 1
					iHSXPanelsPracticeID = SetInt32(alHSXPanelsPracticeID.Item(iWork).ToString)
					If Not oHSX01.HSXPanelsProcessed(iProcessMonth, iProcessYear, iHSXPanelsPracticeID) Then
						moProcessHistory = New ProcessHistory
						CreateHSXPanelFile(iProcessMonth, iProcessYear, iHSXPanelsPracticeID)
					End If
				Next
			Else
				TraceWrite("Waiting for EMR process to complete.", sRoutine)
			End If
		Else
			TraceWrite("Processing not required for HSX Panels export.", sRoutine)
		End If
	End Sub

	Private Sub CreateHSXPanelFile(ProcessMonth As Int32, ProcessYear As Int32, HSXPanelsPracticeID As Int32)
		Dim sRoutine As String = "CreatPanelFile"
		Dim oHSX01 As New HSX01
		Dim iProcessTypeID As Int32 = 6
		Dim oDBC As OleDbConnection = New OleDbConnection
		Dim oCmd As OleDbCommand = New OleDbCommand
		Dim oRdr As OleDbDataReader = Nothing
		Dim bFirstRecord As Boolean
		Dim sPathFileName, sFileName, sOutRecord As String
		Dim iRecordsRead, iRecordsWritten As Int32
		Dim oFileStream As FileStream = Nothing
		Dim oStreamWriter As StreamWriter = Nothing
		Dim sHeader As String = "PATIENTID,DATA_SOURCE_ID,PATIENT_GROUP_ID,FIRST_NAME,MIDDLE_NAME,LAST_NAME,NAME_SUFFIX,ADDRESS_LINE1,ADDRESS_LINE2,CITY,STATE,ZIP,DATE_OF_BIRTH,GENDER,SSN,HOME_PHONE,WORK_PHONE,CELL_PHONE,PROVNAME,ABBREVNAME,INSURANCE"

		If Not moProcessHistory.MarkStart01(iProcessTypeID, mdtCurrentDateTime) Then Throw New Exception(moProcessHistory.ErrorMessage)
		Try
			If Not OpenDB(oDBC, 2) Then Throw New Exception("Error opening database.")
			oCmd.CommandText = "sHSXPanels01"
			oCmd.CommandType = CommandType.StoredProcedure
			oCmd.CommandTimeout = giSQLTimeOutExtended
			oCmd.Parameters.Add("@HSXPanelsPracticeID", OleDbType.BigInt).Value = HSXPanelsPracticeID
			oCmd.Parameters.Add("@ProcessMonth", OleDbType.BigInt).Value = ProcessMonth
			oCmd.Parameters.Add("@ProcessYear", OleDbType.BigInt).Value = ProcessYear
			oCmd.Connection = oDBC
			oRdr = oCmd.ExecuteReader
			bFirstRecord = True
			iRecordsRead = 0
			iRecordsWritten = 0
			If oRdr.HasRows Then
				While oRdr.Read()
					iRecordsRead += 1
					If bFirstRecord Then
						sFileName = SetString(oRdr!FileName) & "_" & ProcessYear.ToString & Format(ProcessMonth, "00") & Format(giHSXPanelsProcessingMonthDay, "00") & ".csv"
						sPathFileName = gsHSXPanelsFileLocation & sFileName
						If Not moProcessHistory.MarkProcessMonth(sPathFileName, ProcessMonth, ProcessYear, HSXPanelsPracticeID) Then Throw New Exception(moProcessHistory.ErrorMessage)
						If File.Exists(sPathFileName) Then
							File.Delete(sPathFileName)
							If File.Exists(sPathFileName) Then Throw New Exception("Unable to remove file " & sPathFileName & "for processing.")
						End If
						oFileStream = New FileStream(sPathFileName, FileMode.Append, FileAccess.Write)
						oStreamWriter = New StreamWriter(oFileStream)
						oStreamWriter.WriteLine(sHeader)
						iRecordsWritten += 1
						bFirstRecord = False
					End If
					sOutRecord = String.Empty
					AddField(sOutRecord, Format(SetDouble(oRdr!PID), "#"), 0, False) 'PATIENTID 
					If sOutRecord = "1549878942050630" Then
						Dim iwork = 0
					End If
					AddField(sOutRecord, SetString(oRdr!Code01), 0, False) 'DATA_SOURCE_ID
					AddField(sOutRecord, SetString(oRdr!Code02), 0, False) 'PATIENT_GROUP_ID
					AddField(sOutRecord, SetString(oRdr!FirstName), 0, True) 'FIRST_NAME'
					AddField(sOutRecord, SetString(oRdr!MiddleName), 0, True) 'MIDDLE_NAME
					AddField(sOutRecord, SetString(oRdr!LastName), 0, True) 'LAST_NAME
					AddField(sOutRecord, SetString(oRdr!Title), 0, False) 'NAME_SUFFIX
					AddField(sOutRecord, SetString(oRdr!Address1), 0, True) 'ADDRESS_LINE1
					AddField(sOutRecord, SetString(oRdr!Address2), 0, True) 'ADDRESS_LINE2
					AddField(sOutRecord, SetString(oRdr!City), 0, True) 'CITY
					AddField(sOutRecord, SetString(oRdr!State), 0, True) 'STATE
					AddField(sOutRecord, SetString(oRdr!ZipCode), 0, False) 'ZIP
					AddField(sOutRecord, SetString(oRdr!DateOfBirth), 0, False) 'DATE_OF_BIRTH
					AddField(sOutRecord, SetString(oRdr!Sex), 0, False) 'GENDER
					AddField(sOutRecord, SetString(oRdr!SocialSecurityNumber), 0, False) 'SSN
					AddField(sOutRecord, SetString(oRdr!HomePhone), 0, False) 'HOME_PHONE
					AddField(sOutRecord, SetString(oRdr!WorkPhone), 11, False) 'WORK_PHONE
					AddField(sOutRecord, SetString(oRdr!CellPhone), 0, False) 'CELL_PHONE
					AddField(sOutRecord, SetString(oRdr!ProviderName), 0, True) 'PROVNAME
					AddField(sOutRecord, SetString(oRdr!AbbreviationName), 0, False) 'ABBREVNAME
					AddField(sOutRecord, SetString(oRdr!Insurance), 0, True) 'INSURANCE
					oStreamWriter.WriteLine(sOutRecord)
					iRecordsWritten += 1
				End While
			Else
				sFileName = oHSX01.FileName(HSXPanelsPracticeID) & "_" & ProcessYear.ToString & ProcessMonth & "01.csv"
				sPathFileName = gsHSXPanelsFileLocation & sFileName
				If Not moProcessHistory.MarkProcessMonth(sPathFileName, ProcessMonth, ProcessYear, HSXPanelsPracticeID) Then Throw New Exception(moProcessHistory.ErrorMessage)
				If File.Exists(sPathFileName) Then
					File.Delete(sPathFileName)
					If File.Exists(sPathFileName) Then Throw New Exception("Unable to remove file " & sPathFileName & "for processing.")
				End If
				oFileStream = New FileStream(sPathFileName, FileMode.Append, FileAccess.Write)
				oStreamWriter = New StreamWriter(oFileStream)
				oStreamWriter.WriteLine(sHeader)
				iRecordsWritten += 1
			End If
			CloseOutput(oStreamWriter, oFileStream)
			If Not moProcessHistory.MarkRecordCount(iRecordsRead, iRecordsWritten) Then Throw New Exception(moProcessHistory.ErrorMessage)
			If Not moProcessHistory.MarkEnd(Now) Then Throw New Exception(moProcessHistory.ErrorMessage)
			TraceWrite("HSX Panels created for HSX Panels Practice ID " & HSXPanelsPracticeID.ToString, sRoutine)
		Catch oException As Exception
			TraceWrite(oException.Message, sRoutine)
		Finally
			CloseRDR(oRdr)
			CloseCMD(oCmd)
			CloseDB(oDBC)
		End Try
	End Sub
#End Region
End Module
