#Region "Init"
Option Strict On
Option Explicit On
Imports System.Data.OleDb
#End Region

Module DBRoutines
#Region "DataBaseConnectionString"
	Public Function DataBaseConnectionString(ByVal DataBaseUser As String, ByVal DatabasePassword As String, ByVal DatabaseName As String, ByVal Server As String) As String
		Return "Provider=SQLOLEDB.1;Persist Security Info=False;User ID=" & DataBaseUser & ";Password=" & DatabasePassword & ";Initial Catalog=" & DatabaseName &
						";Data Source=" & Server & ";Use Procedure for Prepare=1;" & "Auto Translate=True;Packet Size=4096;Workstation ID=AIApplication;Use Encryption for Data=False;" &
						"Tag with column collation when possible=False"
	End Function
#End Region

#Region "OpenDB"
	Public Function OpenDB(DatabaseConnection As OleDbConnection, DBToOpen As Int32) As Boolean
		Try
			Select Case DBToOpen
				Case 1
					DatabaseConnection.ConnectionString = gsHSXConnectionString
				Case 2
					DatabaseConnection.ConnectionString = gsEMRConnectionString
				Case 3
					DatabaseConnection.ConnectionString = gsControllerConnectionString
				Case Else
					Throw New Exception("Invalid Database Type: " & DBToOpen.ToString)
			End Select
			DatabaseConnection.Open()
			OpenDB = True
		Catch oException As Exception
			OpenDB = False
		Finally
		End Try
	End Function
#End Region

#Region "CloseDB"
	Public Sub CloseDB(ByVal cnDB As OleDbConnection)
		Try
			cnDB.Close()
			cnDB.Dispose()
			cnDB = Nothing
		Catch oException As Exception
		End Try
	End Sub
#End Region

#Region "CloseRDR"
	Public Sub CloseRDR(ByVal oRDR As OleDbDataReader)
		Try
			oRDR.Close()
			oRDR = Nothing
		Catch oException As Exception
		End Try
	End Sub
#End Region

#Region "CloseCMD"
	Public Sub CloseCMD(ByVal oCMD As OleDbCommand)
		Try
			oCMD.Dispose()
		Catch oException As Exception
		End Try
	End Sub
#End Region
End Module
