﻿
#Region "Init"
Option Explicit On
Option Strict On
Imports System.Data.OleDb
#End Region

Public Class HSX01
#Region "Declartions"
	Private moDBC As OleDbConnection
#End Region

#Region "Class Properties"
	Public Property HSXRecordTypeID As Int32
	Public Property DestinationFacility As String
	Public Property DestinationPractice As String
	Public Property PrimaryCareProvider As String
	Public Property DestinationMRN As String
	Public Property SourceFacility As String
	Public Property SourceMRN As String
	Public Property FirstName As String
	Public Property MiddleName As String
	Public Property LastName As String
	Public Property Gender As String
	Public Property DateOfBirth As String
	Public Property Address As String
	Public Property City As String
	Public Property State As String
	Public Property Zip As String
	Public Property PrimaryPhone As String
	Public Property SourceSetting As String
	Public Property EventType As String
	Public Property RosterAddedDate As String
	Public Property AdmitDate As String
	Public Property AdmitTime As String
	Public Property AdmitReason As String
	Public Property AdmitType As String
	Public Property ReferralInformation As String
	Public Property DischargeDate As String
	Public Property DischargeTime As String
	Public Property DeathIndicator As String
	Public Property DiagnosisCode As String
	Public Property DiagnosisDescription As String
	Public Property DischargeDisposition As String
	Public Property AttendingDoctor As String
	Public Property Insurance As String
	Public Property CareManagerName As String
	Public Property SubscriberList As String
	Public Property SourceFileName As String
	Public Property ErrorMessage As String
#End Region

#Region "Routines"
	Public Sub New()
		Initialize()
	End Sub

	Public Sub Initialize()
		HSXRecordTypeID = 0
		DestinationFacility = String.Empty
		DestinationPractice = String.Empty
		PrimaryCareProvider = String.Empty
		DestinationMRN = String.Empty
		SourceFacility = String.Empty
		SourceMRN = String.Empty
		FirstName = String.Empty
		MiddleName = String.Empty
		LastName = String.Empty
		Gender = String.Empty
		DateOfBirth = String.Empty
		Address = String.Empty
		City = String.Empty
		State = String.Empty
		Zip = String.Empty
		PrimaryPhone = String.Empty
		SourceSetting = String.Empty
		EventType = String.Empty
		RosterAddedDate = String.Empty
		AdmitDate = String.Empty
		AdmitTime = String.Empty
		AdmitReason = String.Empty
		AdmitType = String.Empty
		ReferralInformation = String.Empty
		DischargeDate = String.Empty
		DischargeTime = String.Empty
		DeathIndicator = String.Empty
		DiagnosisCode = String.Empty
		DiagnosisDescription = String.Empty
		DischargeDisposition = String.Empty
		AttendingDoctor = String.Empty
		Insurance = String.Empty
		CareManagerName = String.Empty
		SubscriberList = String.Empty
		SourceFileName = String.Empty
		ErrorMessage = String.Empty
	End Sub
#End Region

#Region "OpenDatabase"
	Public Function OpenDatabase() As Boolean
		moDBC = New OleDbConnection
		Return OpenDB(moDBC, 2)
	End Function
#End Region

#Region "CloseDatabase"
	Public Sub CloseDatabase()
		CloseDB(moDBC)
	End Sub
#End Region

#Region "Delete"
	Public Function Delete(HSXRecordTypeID As Int32, SourceFileName As String) As Boolean
		Dim oCmd As OleDbCommand = New OleDbCommand

		Try
			oCmd.CommandText = "dHSX01BySourceFileName"
			oCmd.CommandType = CommandType.StoredProcedure
			oCmd.Parameters.Add("@HSXRecordTypeID", OleDbType.BigInt).Value = HSXRecordTypeID
			oCmd.Parameters.Add("@SourceFileName ", OleDbType.VarChar, 100).Value = SourceFileName
			oCmd.Connection = moDBC
			oCmd.ExecuteNonQuery()
			Delete = True
		Catch oException As Exception
			ErrorMessage = oException.Message
			Delete = False
		Finally
			CloseCMD(oCmd)
		End Try
	End Function
#End Region

#Region "Insert"
	Public Function Insert() As Boolean
		Dim oCmd As OleDbCommand = New OleDbCommand

		Try
			oCmd.CommandText = "iHSX01A"
			oCmd.CommandType = CommandType.StoredProcedure
			oCmd.Parameters.Add("@HSXRecordTypeID", OleDbType.BigInt).Value = HSXRecordTypeID
			oCmd.Parameters.Add("@DestinationFacility", OleDbType.VarChar, 250).Value = DestinationFacility
			oCmd.Parameters.Add("@DestinationPractice", OleDbType.VarChar, 250).Value = DestinationPractice
			oCmd.Parameters.Add("@PrimaryCareProvider", OleDbType.VarChar, 250).Value = PrimaryCareProvider
			oCmd.Parameters.Add("@DestinationMRN ", OleDbType.VarChar, 50).Value = DestinationMRN
			oCmd.Parameters.Add("@SourceFacility ", OleDbType.VarChar, 250).Value = SourceFacility
			oCmd.Parameters.Add("@SourceMRN ", OleDbType.VarChar, 50).Value = SourceMRN
			oCmd.Parameters.Add("@FirstName ", OleDbType.VarChar, 100).Value = FirstName
			oCmd.Parameters.Add("@MiddleName", OleDbType.VarChar, 100).Value = MiddleName
			oCmd.Parameters.Add("@LastName ", OleDbType.VarChar, 100).Value = LastName
			oCmd.Parameters.Add("@Gender", OleDbType.VarChar, 25).Value = Gender
			oCmd.Parameters.Add("@DateOfBirth ", OleDbType.VarChar, 10).Value = DateOfBirth
			oCmd.Parameters.Add("@Address ", OleDbType.VarChar, 250).Value = Address
			oCmd.Parameters.Add("@City ", OleDbType.VarChar, 250).Value = City
			oCmd.Parameters.Add("@State", OleDbType.VarChar, 10).Value = State
			oCmd.Parameters.Add("@Zip ", OleDbType.VarChar, 25).Value = Zip
			oCmd.Parameters.Add("@PrimaryPhone ", OleDbType.VarChar, 25).Value = PrimaryPhone
			oCmd.Parameters.Add("@SourceSetting", OleDbType.VarChar, 100).Value = SourceSetting
			oCmd.Parameters.Add("@EventType ", OleDbType.VarChar, 100).Value = EventType
			oCmd.Parameters.Add("@RosterAddedDate ", OleDbType.VarChar, 10).Value = RosterAddedDate
			oCmd.Parameters.Add("@AdmitDate ", OleDbType.VarChar, 10).Value = AdmitDate
			oCmd.Parameters.Add("@AdmitTime ", OleDbType.VarChar, 11).Value = AdmitTime
			If IsDate(AdmitDate & Space(1) & AdmitTime) Then
				oCmd.Parameters.Add("@AdmitDateTime ", OleDbType.Date).Value = CDate(AdmitDate & Space(1) & AdmitTime)
			Else
				oCmd.Parameters.Add("@AdmitDateTime ", OleDbType.Date).Value = DBNull.Value
			End If
			oCmd.Parameters.Add("@AdmitReason ", OleDbType.VarChar, 250).Value = AdmitReason
			oCmd.Parameters.Add("@AdmitType ", OleDbType.VarChar, 25).Value = AdmitType
			oCmd.Parameters.Add("@ReferralInformation", OleDbType.VarChar, 250).Value = ReferralInformation
			oCmd.Parameters.Add("@DischargeDate ", OleDbType.VarChar, 10).Value = DischargeDate
			oCmd.Parameters.Add("@DischargeTime ", OleDbType.VarChar, 11).Value = DischargeTime
			If IsDate(DischargeDate & Space(1) & DischargeTime) Then
				oCmd.Parameters.Add("@DischargeDateTime ", OleDbType.Date).Value = CDate(DischargeDate & Space(1) & DischargeTime)
			Else
				oCmd.Parameters.Add("@DischargeDateTime ", OleDbType.Date).Value = DBNull.Value
			End If
			oCmd.Parameters.Add("@DeathIndicator", OleDbType.VarChar, 5).Value = DeathIndicator
			oCmd.Parameters.Add("@DiagnosisCode ", OleDbType.VarChar, 25).Value = DiagnosisCode
			oCmd.Parameters.Add("@DiagnosisDescription ", OleDbType.VarChar, 250).Value = DiagnosisDescription
			oCmd.Parameters.Add("@DischargeDisposition ", OleDbType.VarChar, 250).Value = DischargeDisposition
			oCmd.Parameters.Add("@AttendingDoctor", OleDbType.VarChar, 250).Value = AttendingDoctor
			oCmd.Parameters.Add("@Insurance ", OleDbType.VarChar, 250).Value = Insurance
			oCmd.Parameters.Add("@CareManagerName", OleDbType.VarChar, 250).Value = CareManagerName
			oCmd.Parameters.Add("@SubscriberList ", OleDbType.VarChar, 512).Value = SubscriberList
			oCmd.Parameters.Add("@SourceFileName ", OleDbType.VarChar, 100).Value = SourceFileName
			oCmd.Connection = moDBC
			oCmd.ExecuteNonQuery()
			Insert = True
		Catch oException As Exception
			ErrorMessage = oException.Message
			Insert = False
		Finally
			CloseCMD(oCmd)
		End Try
	End Function
#End Region

#Region "GetHSXPanelsPracticeID"
	Public Function GetHSXPanelsPracticeID() As ArrayList
		Dim oDBC As OleDbConnection = New OleDbConnection
		Dim oCmd As OleDbCommand = New OleDbCommand
		Dim oRdr As OleDbDataReader = Nothing

		Try
			GetHSXPanelsPracticeID = New ArrayList
			If Not OpenDB(oDBC, 1) Then Throw New Exception("Error opening database.")
			oCmd.CommandText = "sHSXPanelsPracticeID"
			oCmd.CommandType = CommandType.StoredProcedure
			oCmd.Connection = oDBC
			oRdr = oCmd.ExecuteReader
			If oRdr.HasRows Then
				While oRdr.Read()
					GetHSXPanelsPracticeID.Add(SetInt32(oRdr!HSXPanelsPracticeID).ToString)
				End While
			End If
		Catch oException As Exception
			ErrorMessage = oException.Message
			GetHSXPanelsPracticeID = New ArrayList
		Finally
			CloseRDR(oRdr)
			CloseCMD(oCmd)
			CloseDB(oDBC)
		End Try
	End Function
#End Region

#Region "HSXPanelsProcessed"
	Public Function HSXPanelsProcessed(ProcessMonth As Int32, ProcessYear As Int32, HSXPanelsPracticeID As Int32) As Boolean
		Dim oDBC As OleDbConnection = New OleDbConnection
		Dim oCmd As OleDbCommand = New OleDbCommand
		Dim oRdr As OleDbDataReader = Nothing

		Try
			HSXPanelsProcessed = False
			If Not OpenDB(oDBC, 1) Then Throw New Exception("Error opening database.")
			oCmd.CommandText = "sHSXPanelsProcessed"
			oCmd.CommandType = CommandType.StoredProcedure
			oCmd.Parameters.Add("@ProcessMonth ", OleDbType.BigInt).Value = ProcessMonth
			oCmd.Parameters.Add("@ProcessYear ", OleDbType.BigInt).Value = ProcessYear
			oCmd.Parameters.Add("@HSXPanelsPracticeID ", OleDbType.BigInt).Value = HSXPanelsPracticeID
			oCmd.Connection = oDBC
			oRdr = oCmd.ExecuteReader
			HSXPanelsProcessed = oRdr.HasRows
		Catch oException As Exception
			ErrorMessage = oException.Message
			HSXPanelsProcessed = False
		Finally
			CloseRDR(oRdr)
			CloseCMD(oCmd)
			CloseDB(oDBC)
		End Try
	End Function
#End Region

#Region "EMRProcessed"
	Public Function EMRProcessed(ByVal Type As String, ByVal SubType As String) As Boolean
		Dim oDBC As OleDbConnection = New OleDbConnection
		Dim oCmd As OleDbCommand = New OleDbCommand
		Dim oRdr As OleDbDataReader = Nothing
		Dim sRunDate As String = Format$(DateAdd(DateInterval.Day, -1, Now), "yyyy-MM-dd")

		ErrorMessage = vbNullString
		Try
			If Not OpenDB(oDBC, 3) Then Throw New Exception("Processed.Complete: Error opening database.")
			oCmd.CommandText = "sProcessedByTypeSubTypeRunDate"
			oCmd.CommandType = CommandType.StoredProcedure
			oCmd.Parameters.Add("@ServerName", OleDbType.VarChar, 25).Value = gsEMRServer
			oCmd.Parameters.Add("@DatabaseName", OleDbType.VarChar, 25).Value = gsEMRDatabase
			oCmd.Parameters.Add("@Type", OleDbType.VarChar, 100).Value = Type
			oCmd.Parameters.Add("@SubType", OleDbType.VarChar, 100).Value = SubType
			oCmd.Parameters.Add("@RunDate", OleDbType.VarChar, 10).Value = sRunDate
			oCmd.Connection = oDBC
			oRdr = oCmd.ExecuteReader
			oRdr.Read()
			If oRdr.HasRows Then
				EMRProcessed = True
			Else
				EMRProcessed = False
			End If
		Catch oException As Exception
			EMRProcessed = False
			ErrorMessage = oException.Message
		Finally
			CloseRDR(oRdr)
			CloseCMD(oCmd)
			CloseDB(oDBC)
		End Try
	End Function
#End Region

#Region "FileName"
	Public Function FileName(HSXPanelsPracticeID As Int32) As String
		Dim oDBC As OleDbConnection = New OleDbConnection
		Dim oCmd As OleDbCommand = New OleDbCommand
		Dim oRdr As OleDbDataReader = Nothing

		Try
			If Not OpenDB(oDBC, 1) Then Throw New Exception("Error opening database.")
			oCmd.CommandText = "sFileName"
			oCmd.CommandType = CommandType.StoredProcedure
			oCmd.Parameters.Add("@HSXPanelsPracticeID", OleDbType.BigInt).Value = HSXPanelsPracticeID
			oCmd.Connection = oDBC
			oRdr = oCmd.ExecuteReader
			If oRdr.HasRows Then
				oRdr.Read()
				FileName = SetString(oRdr!FileName)
			Else
				FileName = String.Empty
			End If
		Catch oException As Exception
			ErrorMessage = oException.Message
			FileName = String.Empty
		Finally
			CloseRDR(oRdr)
			CloseCMD(oCmd)
			CloseDB(oDBC)
		End Try
	End Function
#End Region
End Class
