﻿#Region "Init"
Option Explicit On
Option Strict On
Imports System.Data.OleDb
#End Region

Public Class ProcessHistory
#Region "Class Properties"
	Public Property ProcessHistoryID() As Int32
	Public Property ErrorMessage As String
#End Region

#Region "MarkStart"
	Public Function MarkStart(ProcessTypeID As Int32, StartDateTime As Date, PathFileName As String) As Boolean
		Dim oDBC As OleDbConnection = New OleDbConnection
		Dim oCmd As OleDbCommand = New OleDbCommand

		MarkStart = False
		Try
			If Not OpenDB(oDBC, 1) Then Throw New Exception("ProcessHistory.MarkStart: Error opening the database.")
			oCmd.CommandText = "iProcessHistory"
			oCmd.CommandType = CommandType.StoredProcedure
			oCmd.Parameters.Add("@ProcessTypeID", OleDbType.BigInt).Value = ProcessTypeID
			oCmd.Parameters.Add("@AccountID", OleDbType.BigInt).Value = giAccountID
			oCmd.Parameters.Add("@StartDateTime", OleDbType.Date).Value = StartDateTime
			oCmd.Parameters.Add("@PathFileName", OleDbType.VarChar, 200).Value = PathFileName
			oCmd.Parameters.Add("@NewID", OleDbType.BigInt).Direction = ParameterDirection.Output
			oCmd.Connection = oDBC
			oCmd.ExecuteNonQuery()
			ProcessHistoryID = SetInt32(oCmd.Parameters("@NewID").Value)
			MarkStart = True
		Catch oException As Exception
			ErrorMessage = oException.Message
			MarkStart = False
		Finally
			CloseCMD(oCmd)
			CloseDB(oDBC)
		End Try
	End Function

	Public Function MarkStart01(ProcessTypeID As Int32, StartDateTime As Date) As Boolean
		Dim oDBC As OleDbConnection = New OleDbConnection
		Dim oCmd As OleDbCommand = New OleDbCommand

		MarkStart01 = False
		Try
			If Not OpenDB(oDBC, 1) Then Throw New Exception("ProcessHistory.MarkStart: Error opening the database.")
			oCmd.CommandText = "iProcessHistory01"
			oCmd.CommandType = CommandType.StoredProcedure
			oCmd.Parameters.Add("@ProcessTypeID", OleDbType.BigInt).Value = ProcessTypeID
			oCmd.Parameters.Add("@AccountID", OleDbType.BigInt).Value = giAccountID
			oCmd.Parameters.Add("@StartDateTime", OleDbType.Date).Value = StartDateTime
			oCmd.Parameters.Add("@NewID", OleDbType.BigInt).Direction = ParameterDirection.Output
			oCmd.Connection = oDBC
			oCmd.ExecuteNonQuery()
			ProcessHistoryID = SetInt32(oCmd.Parameters("@NewID").Value)
			MarkStart01 = True
		Catch oException As Exception
			ErrorMessage = oException.Message
			MarkStart01 = False
		Finally
			CloseCMD(oCmd)
			CloseDB(oDBC)
		End Try
	End Function
#End Region

#Region "MarkRecordCount"
	Public Function MarkRecordCount(RecordsRead As Int32, RecordsWritten As Int32) As Boolean
		Dim oDBC As OleDbConnection = New OleDbConnection
		Dim oCmd As OleDbCommand = New OleDbCommand

		MarkRecordCount = False
		Try
			If Not OpenDB(oDBC, 1) Then Throw New Exception("Error opening the database.")
			oCmd.CommandText = "uProcessHistoryRecordCount"
			oCmd.CommandType = CommandType.StoredProcedure
			oCmd.Parameters.Add("@ProcessHistoryID", OleDbType.BigInt).Value = ProcessHistoryID
			oCmd.Parameters.Add("@RecordsRead", OleDbType.BigInt).Value = RecordsRead
			oCmd.Parameters.Add("@RecordsWritten", OleDbType.BigInt).Value = RecordsWritten
			oCmd.Connection = oDBC
			oCmd.ExecuteNonQuery()
			MarkRecordCount = True
		Catch oException As Exception
			ErrorMessage = "ProcessHistory.MarkRecordCount: " & oException.Message
			MarkRecordCount = False
		Finally
			CloseCMD(oCmd)
			CloseDB(oDBC)
		End Try
	End Function
#End Region

#Region "MarkProcessMonth"
	Public Function MarkProcessMonth(PathFileName As String, ProcessMonth As Int32, ProcessYear As Int32, HSXPanelsPracticeID As Int32) As Boolean
		Dim oDBC As OleDbConnection = New OleDbConnection
		Dim oCmd As OleDbCommand = New OleDbCommand

		MarkProcessMonth = False
		Try
			If Not OpenDB(oDBC, 1) Then Throw New Exception("Error opening the database.")
			oCmd.CommandText = "uProcessHistoryProcessMonth"
			oCmd.CommandType = CommandType.StoredProcedure
			oCmd.Parameters.Add("@ProcessHistoryID", OleDbType.BigInt).Value = ProcessHistoryID
			oCmd.Parameters.Add("@PathFileName", OleDbType.VarChar, 200).Value = PathFileName
			oCmd.Parameters.Add("@ProcessMonth", OleDbType.BigInt).Value = ProcessMonth
			oCmd.Parameters.Add("@ProcessYear", OleDbType.BigInt).Value = ProcessYear
			oCmd.Parameters.Add("@HSXPanelsPracticeID", OleDbType.BigInt).Value = HSXPanelsPracticeID
			oCmd.Connection = oDBC
			oCmd.ExecuteNonQuery()
			MarkProcessMonth = True
		Catch oException As Exception
			ErrorMessage = "ProcessHistory.MarkProcessMonth: " & oException.Message
			MarkProcessMonth = False
		Finally
			CloseCMD(oCmd)
			CloseDB(oDBC)
		End Try
	End Function
#End Region

#Region "MarkEnd"
	Public Function MarkEnd(EndDateTime As Date) As Boolean
		Dim oDBC As OleDbConnection = New OleDbConnection
		Dim oCmd As OleDbCommand = New OleDbCommand

		MarkEnd = False
		Try
			If Not OpenDB(oDBC, 1) Then Throw New Exception("Error opening the database.")
			oCmd.CommandText = "uProcessHistoryMarkEnd"
			oCmd.CommandType = CommandType.StoredProcedure
			oCmd.Parameters.Add("@ProcessHistoryID", OleDbType.BigInt).Value = ProcessHistoryID
			oCmd.Parameters.Add("@EndDateTime", OleDbType.Date).Value = EndDateTime
			oCmd.Connection = oDBC
			oCmd.ExecuteNonQuery()
			MarkEnd = True
		Catch oException As Exception
			ErrorMessage = "ProcessHistory.MarkEnd: " & oException.Message
			MarkEnd = False
		Finally
			CloseCMD(oCmd)
			CloseDB(oDBC)
		End Try
	End Function
#End Region
End Class

