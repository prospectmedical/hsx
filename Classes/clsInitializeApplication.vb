#Region "Init"
Option Explicit On
Option Strict On
Imports System.IO
Imports System.Data.OleDb
#End Region

Public Class InitializeApplication
#Region "Class Properties"
	Public Property LogPath As String
	Public Property Trace As Boolean
	Public Property ServicePauseTime As Int32
	Public Property EMailRecipient As String
	Public Property EMailSender As String
	Public Property SendEMail As Boolean
	Public Property HSXFileLocation As String
	Public Property HSXPanelsFileLocation As String
	Public Property HSXPanelsProcessingTime As ArrayList
	Public Property HSXPanelsProcessingMonthDay As Int32
#End Region

#Region "Initialize"
	Private Sub Initialize()
		LogPath = "C:\Service Logs\HSX\"
		Trace = True
		ServicePauseTime = 60000
		EMailRecipient = vbNullString
		EMailSender = vbNullString
		SendEMail = False
	End Sub
#End Region

#Region "New"
	Public Sub New()
		Initialize()
	End Sub
#End Region

#Region "GetSettings"
	Public Function GetSettings(INIPathFileName As String) As Boolean
		Dim oFileStream As New FileStream(INIPathFileName, FileMode.Open, FileAccess.Read)
		Dim oStreamReader As New StreamReader(oFileStream)
		Dim sInRecord, sItemType, sItemSetting, sWork As String
		Dim iWork As Int32
		Dim iINICounter As Int32 = 0
		Const iINICount As Int32 = 10
		Dim bEMailRecipientFound As Boolean = False
		Dim bHSXPanelsProcessingTimeFound As Boolean = False

		Try
			EMailRecipient = vbNullString
			GetSettings = False
			HSXPanelsProcessingTime = New ArrayList
			oStreamReader.BaseStream.Seek(0, SeekOrigin.Begin)
			While oStreamReader.Peek() > -1
				sInRecord = Trim$(oStreamReader.ReadLine())
				If Len(sInRecord) > 0 Then
					If Mid$(sInRecord, 1, 1) <> ";" Then
						iWork = InStr(1, sInRecord, ";")
						If iWork > 0 Then sInRecord = Trim$(Mid$(sInRecord, 1, iWork - 1))
						iWork = InStr(1, sInRecord, " ")
						If iWork > 3 Then
							If iWork < Len(sInRecord) Then
								sItemType = Trim$(LCase$(Mid$(sInRecord, 1, iWork - 1)))
								sItemSetting = Trim$(Mid$(sInRecord, iWork + 1, Len(sInRecord)))
								Select Case sItemType
									Case "[logpath]"
										LogPath = sItemSetting
										iINICounter += 1
									Case "[trace]"
										If LCase$(SetString(sItemSetting)) = "true" Then
											Trace = True
										Else
											Trace = False
										End If
										iINICounter += 1
									Case "[servicepausetime]"
										ServicePauseTime = SetInt32(sItemSetting) * 60000
										iINICounter += 1
									Case "[emailrecipient]"
										sWork = SetString(sItemSetting)
										If Len(EMailRecipient) > 0 Then
											If Mid(EMailRecipient, Len(EMailRecipient), 1) <> "," AndAlso Mid(EMailRecipient, Len(EMailRecipient), 1) <> ";" Then EMailRecipient = EMailRecipient & ";"
										End If
										EMailRecipient = EMailRecipient & Replace(sWork, ",", ";")
										If Not bEMailRecipientFound Then
											iINICounter += 1
											bEMailRecipientFound = True
										End If
									Case "[emailsender]"
										EMailSender = SetString(sItemSetting)
										iINICounter += 1
									Case "[sendemail]"
										If LCase$(SetString(sItemSetting)) = "true" Then
											SendEMail = True
										Else
											SendEMail = False
										End If
										iINICounter += 1
									Case "[hsxfilelocation]"
										HSXFileLocation = sItemSetting
										iINICounter += 1
									Case "[hsxpanelsfilelocation]"
										HSXPanelsFileLocation = sItemSetting
										iINICounter += 1
									Case "[hsxpanelsprocessingtime]"
										HSXPanelsProcessingTime.Add(sItemSetting)
										If Not bHSXPanelsProcessingTimeFound Then
											iINICounter += 1
											bHSXPanelsProcessingTimeFound = True
										End If
									Case "[hsxpanelsprocessingmonthday]"
										HSXPanelsProcessingMonthDay = SetInt32(sItemSetting)
										iINICounter += 1
								End Select
							End If
						End If
					End If
				End If
			End While
			CloseInput(oStreamReader, oFileStream)
			If iINICount <> iINICounter Then
				WriteEventLog("InitializeApplication", "Error: Incorrect ini count, entries are missing.", EventLogEntryType.Error)
			Else
				GetSettings = True
			End If
		Catch oException As Exception
			WriteEventLog("InitializeApplication", "Error: " & oException.Message, EventLogEntryType.Error)
			GetSettings = False
		Finally
		End Try
	End Function
#End Region

#Region "Version Check"
	Public ReadOnly Property VersionCheck(ByVal Version As String) As Boolean
		Get
			Dim oDBC As OleDbConnection = New OleDbConnection
			Dim oCmd As OleDbCommand = New OleDbCommand
			Dim oRdr As OleDbDataReader = Nothing
			Dim bWork As Boolean

			If Not OpenDB(oDBC, 1) Then Return False
			oCmd.CommandText = "sVersionCheck"
			oCmd.CommandType = CommandType.StoredProcedure
			oCmd.Parameters.Add("@Version", OleDbType.VarChar, 25).Value = Version
			Try
				oCmd.Connection = oDBC
				oRdr = oCmd.ExecuteReader
				If oRdr.HasRows Then
					bWork = True
				Else
					bWork = False
				End If
			Catch oException As Exception
				bWork = False
			Finally
				CloseRDR(oRdr)
				CloseCMD(oCmd)
				CloseDB(oDBC)
			End Try
			Return bWork
		End Get
	End Property
#End Region
End Class