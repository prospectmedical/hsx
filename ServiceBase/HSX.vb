#Region "Init"
Option Explicit On
Option Strict On
Imports System.Threading
#End Region

#Region "Enums"
Public Enum CustomCommands
	StopProcess = 128
	RestartProcess = 129
	CheckProcess = 130
End Enum
#End Region

Public Class HSX
#Region "Declaraions"
	Private Shared moTextWriterTraceListener As TextWriterTraceListener = Nothing
	Private moThread As Thread = Nothing
#End Region

#Region "Routines"

#End Region

#Region "Service Processing"
	Public Sub New()
		InitializeComponent()
		CanPauseAndContinue = False
		ServiceName = "HSX"

		'service process method testing
		'StartUpEMail()
		'If LoadData(1) Then
		'	If LoadData(2) Then
		'		If LoadData(3) Then
		'			If LoadData(4) Then
		'				LoadData(5)
		'			End If
		'		End If
		'	End If
		'End If
		'LoadData(1)
		'LoadData(2)
		'LoadData(3)
		'LoadData(4)
		'LoadData(5)
		'LoadData(6)
		'DoPanels()
		'Trace.Close()
		'ShutDownEMail()
		'End
		'service process method testing
	End Sub

	Private Sub ServiceProcessMethod()
		Dim sRoutine As String = "ServiceProcessMethod"

		Try
			If gbSendEMail Then StartUpEMail()
			WriteEventTrace("Process Messages", sRoutine, EventLogEntryType.Information)
			Do
				If LoadData(1) Then
					If LoadData(2) Then
						If LoadData(3) Then
							If LoadData(4) Then
								If LoadData(5) Then
									LoadData(6)
								End If
							End If
						End If
					End If
				End If
				DoPanels
				TraceWrite(StringRepeat(Chr(127), 100), sRoutine)
				TraceWrite(StringRepeat("-", 100), sRoutine)
				TraceWrite(StringRepeat(Chr(127), 100), sRoutine)
				TraceWrite(Space$(1), sRoutine)
				Thread.Sleep(giServicePauseTime)
			Loop While True
		Catch oThreadAbortException As ThreadAbortException
			WriteEventTrace("Thread abort signaled.", sRoutine, EventLogEntryType.Information)
		Finally
			WriteEventTrace("Exiting the service process thread.", sRoutine, EventLogEntryType.Information)
			Trace.Close()
			If gbSendEMail Then ShutDownEMail()
			moThread.Join(500)
			moThread = Nothing
			Me.Stop()
		End Try
	End Sub
#End Region

#Region "Service Control"
	Protected Overrides Sub OnStart(ByVal args() As String)
		If (moThread Is Nothing) OrElse ((moThread.ThreadState And (System.Threading.ThreadState.Unstarted Or System.Threading.ThreadState.Stopped)) <> 0) Then
			WriteEventTrace("Starting the service process thread.", "OnStart", EventLogEntryType.Information)
			moThread = New Thread(New ThreadStart(AddressOf ServiceProcessMethod))
			moThread.Start()
		End If
		If Not moThread Is Nothing Then
			WriteEventTrace("Process thread state = " & moThread.ThreadState.ToString(), "OnStart", EventLogEntryType.Information)
		End If
	End Sub

	Protected Overrides Sub OnStop()
		If (Not moThread Is Nothing) AndAlso moThread.IsAlive Then
			WriteEventTrace("Stopping the service process thread.", "OnStop", EventLogEntryType.Information)
			moThread.Abort()
			moThread.Join(500)
		End If
		If Not moThread Is Nothing Then
			WriteEventTrace("Process thread state = " & moThread.ThreadState.ToString(), "OnStop", EventLogEntryType.Information)
		End If
	End Sub

	Protected Overrides Sub OnCustomCommand(ByVal command As Integer)
		WriteEventTrace("Custom command received: " & command.ToString(), "OnCustomCommand", EventLogEntryType.Information)
		Select Case command
			Case CustomCommands.StopProcess
				OnStop()
			Case CustomCommands.RestartProcess
				OnStart(Nothing)
			Case CustomCommands.CheckProcess
				WriteEventTrace("Process thread state = " & moThread.ThreadState.ToString(), "OnCustomCommand", EventLogEntryType.Information)
				WriteEventTrace("Inrecognized custom command ignored!", "OnCustomCommand", EventLogEntryType.Information)
		End Select
	End Sub
#End Region
End Class
